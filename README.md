1) Run vagrant:

		cd vagrant_directory
		
		vagrant up
		
		vagrant ssh ansible
		
1) Git clone:

		git clone https://gitlab.com/gpb2/00-ansible.git

3) Run playbook in test server:

		cd 00-ansible/
		
		ansible-playbook test.yml -i inventory.yml